<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MatchingTest extends TestCase
{
    /**
     * Testing status with no JSON petition
     *
     * @return void
     */
    public function testNoJSON()
    {
        $response = $this->get('/api/get-matchings');

        $response->assertStatus(422);
    }

    /**
     * Testing status with valid JSON body
     *
     * @return void
     */
    public function testValidJSON()
    {
        $data = [
            'workers' => [
                [
                    "id"=> 1,
                    "availability" => ["Monday", "Wednesday"],
                    "payrate" => 7.50
                ],
                [
                    "id"=> 2,
                    "availability" =>  ["Monday", "Tuesday", "Thursday"],
                    "payrate" => 9.00
                ]
            ],
            "shifts" => [
                [
                    "id" => 1,
                    "day" => ["Monday"]
                ],
                [
                    "id" => 2,
                    "day" => ["Tuesday"]
                ]
            ]
        ];
        $response = $this->json('GET', '/api/get-matchings', $data
            ,
            ['contentType' => 'application/json']);

        $response->assertStatus(200);
    }

    /**
     * Testing status with JSON with 7 validation errors:
     *  -   workers.0.id            duplicated
     *  -   workers.0.availability  "Test" is not a valid day
     *  -   workers.0.payrate       Not valid format
     *  -   workers.1.id            duplicated
     *  -   shifts.0.id             duplicated
     *  -   shifts.0.day            "Test" is not a valid day
     *  -   shifts.1.id             duplicated
     *
     * @return void
     */
    public function testValidationErrors()
    {
        $data = [
            'workers' => [
                [
                    "id"=> 1,
                    "availability" => ["Test"],
                    "payrate" => 7.50
                ],
                [
                    "id"=> 1,
                    "availability" =>  ["Monday", "Tuesday", "Thursday"],
                    "payrate" => 9.001
                ]
            ],
            "shifts" => [
                [
                    "id" => 1,
                    "day" => ["Test"]
                ],
                [
                    "id" => 1,
                    "day" => ["Tuesday"]
                ]
            ]
        ];
        $response = $this->json('GET', '/api/get-matchings', $data,
            ['contentType' => 'application/json']);

        $response->assertJsonValidationErrors(6);
    }

    public function testFullMatching() {
        $data = json_decode(file_get_contents(__DIR__ . '/json/test1.json'), true);
        $response = $this->json('GET', '/api/get-matchings', $data,
            ['contentType' => 'application/json']);

        $response->assertJson([
            'data' => [
                'matchings' => true,
                'workers'   => [
                    'matched' => 4,
                ]
            ]
        ]);
    }

    public function testPartialMatching() {
        $data = json_decode(file_get_contents(__DIR__ . '/json/test2.json'), true);
        $response = $this->json('GET', '/api/get-matchings', $data,
            ['contentType' => 'application/json']);

        $response->assertJson([
            'data' => [
                'matchings' => true,
                'workers'   => [
                    'matched' => 8,
                ]
            ]
        ]);
    }
}
