## Synopsis

Laravel project with a single API service to match a list of workers with a list of available shifts by day.

## Usage

GET petition to /api/get-matchings with JSON body containing at least one worker and one shift to match.
Option "?force=1" has been added to force the use of a recursive method to get the best result available. **Not recomended** on large lists.

JSON example:

{
  "workers": [
    {
      "id": 1,
      "availability": ["Monday", "Wednesday"],
      "payrate": 7.50
    }
  ],
  "shifts": [
    {
      "id": 1,
      "day": ["Wednesday"]
    }
  ]
}

## Installation

**Dockerfile** has been included to easily build a container with the required service. The port to serve is given by $PORT ENV variable, required to work in Heroku.

## Tests 

Several PHPUnit tests have been added to verify that the service is working before production deployment. Recommended for continuous integration/deployment (Jenkings, for example).
To manually execute tests, run "/vendor/bin/phpunit".

## Code

The core files to check how this service works are:

 * /app/Http/Controllers/MatchingController.php: validation and service functionality
 * /routes/api.php: routing
 * /tests/Feature/MatchingTest.php: Unit tests to verify matching service
 
## Author
* **Julian Vera** - [Web](https://www.asuraprojects.es/)

