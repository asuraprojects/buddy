# Docker file to Laravel	
FROM  php:7.2
MAINTAINER Julian Vera

RUN apt-get update && apt-get install -y libmcrypt-dev git \ 
    mysql-client zip unzip libmagickwand-dev --no-install-recommends \ 
    && pecl install imagick \
    && pecl install mcrypt-1.0.1 \
    && pecl install xdebug \
    && docker-php-ext-enable imagick \
    && docker-php-ext-enable mcrypt \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-install pdo_mysql \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer 

# Add Bin Composer in PATH
ENV PATH ${PATH}:~/.composer/vendor/bin
WORKDIR /var/www/html
COPY --chown=www-data:www-data . /var/www/html
RUN composer install

CMD php artisan serve --host=0.0.0.0 --port=$PORT
