<?php
/**
 * MatchingController focuses on matching workers and shifts by day.
 *
 * getMatchings() is the function called by the router ("/api/get-matchings")
 *
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MatchingController extends Controller
{
    /**
     * Function that recieves a list of workers and a list of shifts
     * and matches worker - shift matchings
     *
     * This function uses the iterative method getMatchingsList() to
     * obtain the matchings list, beign able to use the recursive
     * method if the user wants to force to (?force=1)
     *
     * @return array JSON
     */
    public function getMatchings(Request $request) {


        $data = json_decode($request->getContent(), true);

        if ($data == null)
            return response()->json(['data' => '', 'errors' => [json_last_error_msg()]], 422);

        $rules = [
            'workers' => 'required',
            'workers.*.id' => ['required', 'integer', 'distinct'],
            'workers.*.availability.*' => [Rule::in('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday')],
            'workers.*.payrate' => "required|regex:/^\d*(\.\d{1,2})?$/",
            'shifts' => 'required',
            'shifts.*.id' => ['required', 'integer', 'distinct'],
            'shifts.*.day.*' => [Rule::in('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday')],
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return response()->json(['data' => '', 'errors' => $validator->errors()->all()], 422);
        }

        $this->daysToInt($data['workers'], $data['shifts']);

        $matchings = array();
        $counter = 0;
        if ($request->input('force') == 1)
            $this->getRecursiveMatchingsList($data['workers'], $data['shifts'], $matchings, $counter);
        else
            $matchings = $this->getMatchingsList($data['workers'], $data['shifts'],$counter);



        $paired_workers = $this->countWorkersFromMatchings($matchings);
        $paired_shifts = count($matchings);
        $total_payment = $this->getMatchingsPayment($matchings);

        $response = [
            'matchings' => $matchings,
            'workers'   => [
                'matched'    => $paired_workers,
                'unmatched'      => (count($data['workers']) - $paired_workers),
                'total'     => count($data['workers']),
                'ratio'     => (count($data['workers']) > 0)?($paired_workers / count($data['workers'])):0
            ],
            'shifts'    => [
                'matched'    => $paired_shifts,
                'unmatched'      => (count($data['shifts']) - $paired_shifts),
                'total'     => count($data['shifts']),
                'ratio'     => (count($data['shifts']) > 0)?($paired_shifts / count($data['shifts'])):0
            ],
            'payment'   => [
                'total'     => $total_payment,
                'average'   => ($paired_shifts > 0)?($total_payment / $paired_shifts):0
            ],
            'stats'     => [
                'iterations'=> $counter,
            ]
        ];

        if ($response['workers']['ratio'] == 1)
            $message = "Every worker has at least one match";
        else if ($response['workers']['ratio'] > 0)
            $message = "At least one worker has not shift available";
        else
            $message = "No matches";

        return response()->json(['message' => $message, 'data' => $response], 200);
    }

    /**
     * Function that calculates recursively the best matching list. Not advisable on
     * large workers/shifts lists.
     *
     * $workers         Pending workers list, beign removed as long as they
     *                  dont have any matchable day
     * $shifts          Pending shifts list
     * $matchings       Matchings list
     * $counter         Iterations counter
     *
     * @return array
     */
    private function getRecursiveMatchingsList(&$workers, &$shifts, &$matchings, &$counter) {
        $counter += 1;
        $matchings_list = array();

        foreach($workers as $key=>$worker) {
            foreach ($workers[$key]['availability'] as $day_key => $day) {
                $available_shifts = $this->findShiftKeysByDay($day, $shifts);
                if (!empty($available_shifts)) {
                    foreach ($available_shifts as $shift_key) {
                        $new_matchings = array_merge($matchings, [[
                            'worker' => $workers[$key]['id'],
                            'shift' => $shifts[$shift_key]['id'],
                            'payrate' => $workers[$key]['payrate']
                        ]]);

                        $new_workers = $workers;
                        unset($new_workers[$key]['availability'][$day_key]);
                        if (empty($new_workers[$key]['availability'])) {
                            unset($new_workers[$key]);
                        }

                        if (!empty($new_workers)) {
                            $new_shifts = $shifts;
                            unset($new_shifts[$shift_key]);
                            $this->getRecursiveMatchingsList($new_workers, $new_shifts, $new_matchings, $counter);
                            $matchings_list[] = $new_matchings;
                        }
                    }
                }
            }
        }
        $matchings_list[] = $matchings;
        $matchings = $this->getBestMatchingsList($matchings_list);
        return $matchings;
    }

    /**
     * Function that given a list of workers and shifts, returns the matching
     * list.
     *
     * Looks for the worker with less chances to pick a shift, then assigns a shift
     * in the day with the worst "count(shifts)/count(workers able to pick a shift that day)"
     * ratio.
     *
     * Each iteration refreshes the pondered values of each day and removes
     * worker['availability'] and shift from their lists when a match is found.
     * If a worker is not able to do any match, is removed from list.
     *
     * @return array
     */
    private function getMatchingsList($workers, $shifts, &$counter) {
        $matchings = array();
        foreach($workers as $worker_key=>$worker) {
            $workers[$worker_key]['matched'] = 0;
        }

        while (!empty($workers)) {
            $counter += 1;
            $pondered_values = $this->getPonderedValues($workers, $shifts);
            $worker_key = $this->getNextWorker($workers, $pondered_values);
            $shift_key = $this->matchWorker($workers[$worker_key], $shifts, $pondered_values);
            if ($shift_key >= 0) {
                $matchings[] = [
                    'worker' => $workers[$worker_key]['id'],
                    'shift' => $shifts[$shift_key]['id'],
                    'payrate' => $workers[$worker_key]['payrate']
                ];
                $workers[$worker_key]['matched'] = 1;
                foreach($workers[$worker_key]['availability'] as $day_key=>$day) {
                    if ($day == $shifts[$shift_key]['day']) {
                        unset($workers[$worker_key]['availability'][$day_key]);
                        if (empty($workers[$worker_key]['availability']))
                            unset($workers[$worker_key]);
                    }
                }
                unset($shifts[$shift_key]);

            } else {
                unset($workers[$worker_key]);
            }
        }

        return $matchings;
    }

    /**
     * Function that recieves a list of matchings and counts how many
     * workers have been matched
     *
     * @return Integer
     */
    private function countWorkersFromMatchings($matchings) {
        $workers = array();
        foreach ($matchings as $matching) {
            if (!in_array($matching['worker'], $workers)){
                $workers[] = $matching['worker'];
            }
        }
        return count($workers);
    }

    /**
     * Function that recieves an array of matchings lists to compare
     * them and return the best matching list by criteria
     *
     * @return array
     */
    private function getBestMatchingsList($matchings_list) {
        $best_matching = array();
        $best_matching_workers = 0;
        foreach($matchings_list as $matching) {
            $matching_workers = $this->countWorkersFromMatchings($matching);
            if ($matching_workers > $best_matching_workers ||
                ($matching_workers == $best_matching_workers && count($matching) > count($best_matching))) {
                $best_matching = $matching;
                $best_matching_workers = $matching_workers;
            }
        }
        return $best_matching;
    }

    /**
     * Function that lists shifts available in $day.
     *
     * Note: this function doesn't return a list of shifts
     * but the keys from current shifts array just for
     * efficency reasons
     *
     * @return array
     */
    private function findShiftKeysByDay($day, $shifts) {
        $foundShifts = array();
        foreach($shifts as $key=>$shift) {
            foreach($shift['day'] as $shift_day) {
                if ($shift_day == $day) {
                    $foundShifts[] = $key;
                }
            }
        }
        return $foundShifts;
    }

    /**
     * Function that calculates full payment of a matching list
     * given
     *
     * @return float
     */
    private function getMatchingsPayment($matchings) {
        $total = 0;
        if (!empty($matchings)) {
            foreach($matchings as $matching) {
                $total += $matching['payrate'];
            }
        }
        return $total;
    }

    /**
     * Function that converts days Strings to Integers of $workers and
     * $shifts lists to avoid String comparison
     *
     * @return void
     */
    private function daysToInt(&$workers, &$shifts) {
        foreach ($workers as $key=>$worker) {
            foreach($worker['availability'] as $day_key =>$day) {
                $workers[$key]['availability'][$day_key] = $this->dayToInt($day);
            }
        }
        foreach ($shifts as $key=>$shift) {
            foreach($shift['day'] as $day_key =>$day) {
                $shifts[$key]['day'][$day_key] = $this->dayToInt($day);
            }
        }
    }

    /**
     * Function that returns $day as Integer
     *
     * @return Integer
     */
    private function dayToInt($day2) {
        switch($day2) {
            case 'Monday':
                $day = 1;
                break;
            case 'Tuesday':
                $day = 2;
                break;
            case 'Wednesday':
                $day = 3;
                break;
            case 'Thursday':
                $day = 4;
                break;
            case 'Friday':
                $day = 5;
                break;
            default:
                $day = 0;
        }
        return $day;
    }

    /**
     * Function that returns an array with a ponderation about
     * how easy is to assign a shift each day. The higher, the
     * easier. This ponderation relates the number of shifts
     * available with the number of workers that can take a shift
     * that day.
     *
     * @return array
     */
    private function getPonderedValues($workers, $shifts) {
        $shift_day_occurrences = [0, 0, 0, 0, 0, 0, 0];
        $worker_day_ocurrences = [0, 0, 0, 0, 0, 0, 0];
        $shift_worker_day_ocurrences = [0, 0, 0, 0, 0, 0, 0];

        $workers_count = count($workers);
        $shifts_count = count($shifts);

        foreach($shifts as $shift) {
            foreach($shift['day'] as $day)
                $shift_day_occurrences[$day] += 1;
        }

        foreach ($workers as $key=>$worker) {
            foreach($worker['availability'] as $day) {
                $worker_day_ocurrences[$day] += 1;
            }
        }

        foreach($shift_worker_day_ocurrences as $key=>$value) {
            $shift_worker_day_ocurrences[$key] = ($worker_day_ocurrences[$key] > 0)?($shift_day_occurrences[$key] / $worker_day_ocurrences[$key]):0;
        }
        return $shift_worker_day_ocurrences;
    }

    /**
     * Function that returns the key of the worker with less chances to
     * pick a shift.
     *
     * @return integer
     */
    private function getNextWorker($workers, $shift_worker_day_ocurrences) {
        $chance = PHP_INT_MAX;
        $payrate = PHP_INT_MAX;
        $matched = 1;
        $result_key = 0;


        foreach($workers as $key=>$worker) {
            $workers[$key]['chance'] = 0;
            foreach($worker['availability'] as $day) {
                $workers[$key]['chance'] += $shift_worker_day_ocurrences[$day];
            }
            if (($workers[$key]['matched'] < $matched) ||
                (($workers[$key]['matched'] == $matched) && ($workers[$key]['chance'] < $chance || ($workers[$key]['chance'] == $chance && $workers[$key]['payrate'] < $payrate)))) {
                $result_key = $key;
                $matched = $workers[$key]['matched'];
                $chance = $workers[$key]['chance'];
                $payrate = $workers[$key]['payrate'];

            }
        }
        return $result_key;
    }

    /**
     * Function that matches a worker with a shift in the day with more,
     * options to be picked. This ponderation is previously calculated by
     * $this->getPonderedValues() function. Returns the shift index/key or
     * -1 if not found.
     *
     * @return integer
     */
    private function matchWorker(&$worker, &$shifts, $shift_worker_day_ocurrences) {
        $best_day = 0;
        $best_day_chance_relation = PHP_INT_MAX;
        foreach($worker['availability'] as $day) {
            if (($shift_worker_day_ocurrences[$day] < $best_day_chance_relation) && $shift_worker_day_ocurrences[$day] > 0) {
                $best_day = $day;
                $best_day_chance_relation = $shift_worker_day_ocurrences[$day];
            }

        }
        $available_shifts = $this->findShiftKeysByDay($best_day, $shifts);
        return (isset($available_shifts[0]))?$available_shifts[0]:-1;
    }
}

